package com.company.algorithms;

public class BubbleSort extends Method {

    public BubbleSort(int[] arr) {
        super(arr);
    }

    public BubbleSort() {
    }

    @Override
    public void sortMin(int[] arr) {
        if (isValidate(arr)) {
            for (int i = arr.length; i > 0; i--) {
                for (int j = 0; j < i - 1; j++) {
                    if (arr[j] < arr[j + 1]) {
                        swap(arr, j);
                    }
                }
            }
        }
        setArrMethod(arr);
    }

    @Override
    public void sortMax(int[] arr) {
        if (isValidate(arr)) {
            for (int i = arr.length; i > 0; i--) {
                for (int j = 0; j < i - 1; j++) {
                    if (arr[j] > arr[j + 1]) {
                        swap(arr, j);
                    }
                }
            }
        }
        setArrMethod(arr);
    }

    private void swap(int[] arrayNumber, int indexFirstNumber) {
        int temp = arrayNumber[indexFirstNumber];
        arrayNumber[indexFirstNumber] = arrayNumber[indexFirstNumber + 1];
        arrayNumber[indexFirstNumber + 1] = temp;
    }
}
