package com.company.algorithms;

public class InsertSort extends Method {

    public InsertSort(int[] arr) {
        super(arr);
    }

    public InsertSort() {
    }

    @Override
    public void sortMax(int[] arr) {
        if (isValidate(arr)) {
            for (int i = 0; i < arr.length; i++) {
                int index = i;
                int temp = arr[i];

                while (index > 0 && arr[index - 1] >= temp) {
                    arr[index] = arr[index - 1];
                    index--;
                }
                arr[index] = temp;
            }
            setArrMethod(arr);
        }
    }

    @Override
    public void sortMin(int[] arr) {
        if (isValidate(arr)) {
            for (int i = 0; i < arr.length; i++) {
                int index = i;
                int temp = arr[i];

                while (index > 0 && arr[index - 1] <= temp) {
                    arr[index] = arr[index - 1];
                    index--;
                }
                arr[index] = temp;
            }
            setArrMethod(arr);
        }
    }


}
