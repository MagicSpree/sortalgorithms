package com.company.algorithms;

import java.util.Arrays;

public abstract class Method {

    private int[] arrMethod;
    private boolean isValid = true;

    Method(int[] arr) {
        this.arrMethod = arr;
    }

    Method() {
    }

    public void sortMax() {
        sortMax(arrMethod);
    }

    public void sortMin() {
        sortMin(arrMethod);
    }

    public abstract void sortMax(int[] arr);

    public abstract void sortMin(int[] arr);

    public int[] getArr() {
        return arrMethod;
    }

    public void setArrMethod(int[] arrMethod) {
        this.arrMethod = arrMethod;
    }

    public boolean isValidate(int[] arr) {
        if (arr == null) {
            print("Array can not be null");
            isValid = false;
        } else if (arr.length == 0) {
            print("Array can not be empty");
            isValid = false;
        }
        return isValid;
    }

    public String toString() {
        return isValid ? Arrays.toString(arrMethod) : "Ошибка сортировки массива";
    }

    private void print(String msg) {
        System.out.println(msg);
    }

}
