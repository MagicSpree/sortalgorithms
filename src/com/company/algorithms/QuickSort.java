package com.company.algorithms;

public class QuickSort extends Method {

    public QuickSort(int[] arr) {
        super(arr);
    }

    public QuickSort() {
    }

    @Override
    public void sortMax(int[] arr) {
        if (isValidate(arr)) {
            quickSortMax(arr, 0, arr.length - 1);
        }
        setArrMethod(arr);
    }

    @Override
    public void sortMin(int[] arr) {
        if (isValidate(arr)) {
            quickSortMin(arr, 0, arr.length - 1);
        }
        setArrMethod(arr);
    }

    private void quickSortMax(int[] source, int leftBorder, int rightBorder) {
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        int pivot = source[(leftMarker + rightMarker) / 2];
        do {

            while (source[leftMarker] < pivot) {
                leftMarker++;
            }

            while (source[rightMarker] > pivot) {
                rightMarker--;
            }

            if (leftMarker <= rightMarker) {
                int tmp = source[leftMarker];
                source[leftMarker] = source[rightMarker];
                source[rightMarker] = tmp;
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < rightBorder) {
            quickSortMax(source, leftMarker, rightBorder);
        }

        if (leftBorder < rightMarker) {
            quickSortMax(source, leftBorder, rightMarker);
        }
    }

    private void quickSortMin(int[] source, int leftBorder, int rightBorder) {
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        int pivot = source[(leftMarker + rightMarker) / 2];
        do {

            while (source[leftMarker] > pivot) {
                leftMarker++;
            }

            while (source[rightMarker] < pivot) {
                rightMarker--;
            }

            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    int tmp = source[leftMarker];
                    source[leftMarker] = source[rightMarker];
                    source[rightMarker] = tmp;
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < rightBorder) {
            quickSortMin(source, leftMarker, rightBorder);
        }

        if (leftBorder < rightMarker) {
            quickSortMin(source, leftBorder, rightMarker);
        }
    }
}
