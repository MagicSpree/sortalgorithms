package com.company.algorithms;

public class SelectionSort extends Method {

    public SelectionSort(int[] arr) {
        super(arr);
    }

    public SelectionSort() {
    }

    public void sortMax(int[] arr) {
        if (isValidate(arr)) {
            for (int i = 0; i < arr.length; i++) {
                int minValue = arr[i];
                int minIndex = i;

                for (int j = i + 1; j < arr.length; j++) {
                    if (arr[j] < minValue) {
                        minValue = arr[j];
                        minIndex = j;
                    }
                }
                if (i != minIndex) {
                    swap(arr, i, minIndex);
                }
            }
            setArrMethod(arr);
        }
    }

    @Override
    public void sortMin(int[] arr) {
        if (isValidate(arr)) {
            for (int i = 0; i < arr.length; i++) {
                int minValue = arr[i];
                int minIndex = i;

                for (int j = i + 1; j < arr.length; j++) {
                    if (arr[j] > minValue) {
                        minValue = arr[j];
                        minIndex = j;
                    }
                }
                if (i != minIndex) {
                    swap(arr, i, minIndex);
                }
            }
            setArrMethod(arr);
        }
    }

    private void swap(int[] arrayNumber, int indexFirstNumber, int indexSecondNumber) {
        int temp = arrayNumber[indexFirstNumber];
        arrayNumber[indexFirstNumber] = arrayNumber[indexSecondNumber];
        arrayNumber[indexSecondNumber] = temp;
    }
}
