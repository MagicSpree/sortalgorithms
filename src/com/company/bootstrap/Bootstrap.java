package com.company.bootstrap;

import com.company.algorithms.*;

public class Bootstrap {

    Method method;
    private int[] arrayNumber = {98, 14, 31, 6, 0, 41, -11};

    public void run() {
        createObjectBubble();
        createObjectSelect();
        createObjectInsert();
        createObjectQuick();
    }

    private void createObjectBubble() {
        method = new BubbleSort(arrayNumber);
        print("Сортировка пузырьком");
        getSort(method);
        method = new BubbleSort();
        getSortWithArray(method, arrayNumber);
    }

    private void createObjectSelect() {
        method = new SelectionSort(arrayNumber);
        print("Сортировка выбором");
        getSort(method);
        method = new SelectionSort();
        getSortWithArray(method, arrayNumber);
    }

    private void createObjectInsert() {
        method = new InsertSort(arrayNumber);
        print("Сортировка вставками");
        getSort(method);
        method = new InsertSort();
        getSortWithArray(method, arrayNumber);
    }

    private void createObjectQuick() {
        method = new QuickSort(arrayNumber);
        print("Быстрая сортировка");
        getSort(method);
        method = new QuickSort();
        getSortWithArray(method, arrayNumber);
    }

    private void getSort(Method method) {
        print("По возрастанию");
        method.sortMax();
        print(method.toString());
    }

    private void getSortWithArray(Method method, int[] array) {
        print("По убыванию");
        method.sortMin(array);
        print(method + "\n");
    }

    private void print(String message) {
        System.out.println(message);
    }
}
